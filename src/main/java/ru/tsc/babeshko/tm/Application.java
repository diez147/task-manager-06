package ru.tsc.babeshko.tm;

import ru.tsc.babeshko.tm.constant.ArgumentConst;
import ru.tsc.babeshko.tm.constant.TerminalConst;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    private static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showErrorArgument(final String arg) {
        System.err.printf("Error! This argument '%s' not supported...\n", arg);
    }

    public static void showErrorCommand(final String arg) {
        System.err.printf("Error! This command '%s' not supported...\n", arg);
    }

    public static void showWelcome() {
        System.err.println("** WELCOME TO TASK-MANAGER **");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show program version. \n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info. \n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show list of terminal commands. \n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application. \n", TerminalConst.EXIT);
    }

    private static void showVersion() {
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Oleg Babeshko");
        System.out.println("Diez147@gmail.com");
    }

    private static void exit() {
        System.exit(0);
    }

}